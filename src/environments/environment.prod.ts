export const environment = {
  production: true,
  API_URL_USERS: 'http://localhost:8080/secure/users',
  API_URL_BULLETINS: 'http://localhost:8888/secure/bulletins',
  API_URL_COMMENTS: 'http://localhost:8888/secure/comments',
};
