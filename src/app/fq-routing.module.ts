import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FqHomeComponent} from "./shared/home/fq-home.component";
import {FqAuthGuard} from "./shared/guards/fq-auth.guard";

const routes: Routes = [
  {path: '', component: FqHomeComponent},
  {
    path: 'public',
    loadChildren: () => import('./public/components/fq-public.module').then(m => m.FqPublicModule)
  },
  {
    path: 'bulletins',
    loadChildren: () => import('./secure/bulletin/fq-bulletin.module').then(m => m.FqBulletinModule),
    canLoad: [FqAuthGuard]
  },
  {
    path: '**', redirectTo: '/bulletins', pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class FqRoutingModule { }
