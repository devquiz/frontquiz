import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {FqRoutingModule} from './fq-routing.module';
import {FqComponent} from './fq.component';
import {FqSharedModule} from './shared/fq-shared.module';
import {ToastrModule} from "ngx-toastr";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {UserStorageService} from "./integrations/service/user-storage.service";

@NgModule({
  declarations: [
    FqComponent,
  ],
  imports: [
    BrowserModule,
    FqRoutingModule,
    FqSharedModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    UserStorageService
  ],
  bootstrap: [FqComponent]
})
export class FqModule { }
