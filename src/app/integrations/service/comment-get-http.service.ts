import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Comment} from "../models/Comment.model";
import {environment} from "../../../environments/environment";

@Injectable()
export class CommentGetHttpService {

  constructor(private _httpClient: HttpClient) {
  }

  getComments(bulletinId: number): Observable<Comment[]> {
    return this._httpClient.get<Comment[]>(environment.API_URL_COMMENTS + `/${bulletinId}`);
  }
}
