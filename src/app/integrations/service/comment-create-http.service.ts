import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Comment} from "../models/Comment.model";
import {environment} from "../../../environments/environment";

@Injectable()
export class CommentCreateHttpService {

  constructor(private _httpClient: HttpClient) {
  }

  createComment(text: string, accountId: string, userId: string, bulletinId: number): Observable<Comment> {
    const body = {
      "content": text
    };
    return this._httpClient.post<Comment>(environment.API_URL_COMMENTS + `/${bulletinId}`, body, {
      headers: {
        'account-id': accountId,
        'user-id': userId
      }
    });
  }
}
