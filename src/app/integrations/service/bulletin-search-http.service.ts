import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Pagination} from "../models/Pagination-Bulletin.model";
import {environment} from "../../../environments/environment";

@Injectable()
export class BulletinSearchHttpService {

  constructor(private _httpClient: HttpClient) {
  }

  searchBulletins(accountId: string, userId: string, text: string, limit: number, page: number): Observable<Pagination> {
    const params = new HttpParams()
      .append('limit', limit)
      .append('page', page);

    const body = {
      "body": text
    };

    return this._httpClient.post<Pagination>(environment.API_URL_BULLETINS + '/search', body, {
      headers: {
        'account-id': accountId,
        'user-id': userId,
        'Content-Type': 'application/json'
      }, params
    });
  }
}
