import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Comment} from "../models/Comment.model";
import {environment} from "../../../environments/environment";

@Injectable()
export class CommentReplyCreateHttpService {

  constructor(private _httpClient: HttpClient) {
  }

  replyComment(text: string, accountId: string, userId: string, commentId: number): Observable<Comment> {
    const body = {
      "content": text
    };
    return this._httpClient.post<Comment>(environment.API_URL_COMMENTS + `/reply/${commentId}`, body, {
      headers: {
        'account-id': accountId,
        'sender-user-id': userId
      }
    });
  }
}
