import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../models/User.model';

@Injectable()
export class UserStorageService {

  public currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  private readonly _USER_KEY = 'user';

  constructor(private _router: Router) {
    // @ts-ignore
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get userValue(): User {
    return this.currentUserSubject.value;
  }

  public logout(): void {
    localStorage.clear();
    // @ts-ignore
    this.currentUserSubject.next(null);
    this._router.navigate(['/public/sign-in']);
  }

  public saveUser(user: User): void {
    localStorage.removeItem(this._USER_KEY);
    localStorage.setItem(this._USER_KEY, JSON.stringify(user));
  }
}
