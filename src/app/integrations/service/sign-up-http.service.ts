import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../models/User.model";
import {Observable} from "rxjs";
import {NgForm} from "@angular/forms";
import {environment} from "../../../environments/environment";

@Injectable()
export class SignUpHttpService {

  constructor(private _httpClient: HttpClient) {
  }

  signUp(form: NgForm): Observable<User> {
    return this._httpClient.post<User>(environment.API_URL_USERS, form);
  }
}
