import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Bulletin} from "../models/Bulletin.model";
import {environment} from "../../../environments/environment";

@Injectable()
export class BulletinCreateHttpService {

  constructor(private _httpClient: HttpClient) {
  }

  createBulletin(text: string, fileIds: string[], userId: string, accountId: string): Observable<Bulletin> {
    const body = {
      "content": text,
      "fileIds": fileIds
    }
    return this._httpClient.post<Bulletin>(environment.API_URL_BULLETINS, body, {
      headers: {
        'account-id': accountId,
        'user-id': userId
      }
    });
  }
}
