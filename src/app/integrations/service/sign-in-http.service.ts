import {Injectable} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Observable} from "rxjs";
import {User} from "../models/User.model";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class SignInHttpService {

  constructor(private _httpClient: HttpClient) {
  }

  signIn(form: NgForm): Observable<User> {
    return this._httpClient.post<User>(environment.API_URL_USERS + "/login", form);
  }
}
