import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Bulletin} from "../models/Bulletin.model";
import {environment} from "../../../environments/environment";

@Injectable()
export class BulletinGetHttpService {

  constructor(private _httpClient: HttpClient) {
  }

  getBulletins(): Observable<Bulletin[]> {
    return this._httpClient.get<Bulletin[]>(environment.API_URL_BULLETINS);
  }
}
