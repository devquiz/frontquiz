export interface User {
  userId: number;
  accountId: number;
  firstName: string;
  lastName: string;
  createdDate: Date;
  isDeleted: boolean;
}