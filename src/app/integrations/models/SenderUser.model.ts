export interface SenderUser {
  userId: number;
  accountId: number;
  name1: string;
  name2: string;
  createdDate: Date;
  isDeleted: boolean;
}
