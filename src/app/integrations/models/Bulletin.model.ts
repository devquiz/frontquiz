import {SenderUser} from "./SenderUser.model";

export interface Bulletin {
  bulletinId: number;
  accountId: number;
  senderUser: SenderUser;
  body: string;
  createdDate: Date;
  isDeleted: boolean;
  commentsCounter: number;
}
