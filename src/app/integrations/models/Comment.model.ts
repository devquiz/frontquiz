import {SenderUser} from "./SenderUser.model";

export interface Comment {
  commentId: number;
  accountId: number;
  senderUser: SenderUser;
  bulletinId: number;
  text: string;
  parentId: number;
  repliesCounter: number;
  isDeleted: boolean;
  createdDate: Date;
}
