import {Bulletin} from "./Bulletin.model";

export interface Pagination {
  content: Bulletin[];
  totalElement: number;
  totalPages: number;
}
