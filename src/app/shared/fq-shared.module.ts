import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FqNavbarComponent} from './navbar/fq-navbar.component';
import {FqHomeComponent} from "./home/fq-home.component";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {FqCommentInputComponent} from './comments/comment-input/fq-comment-input.component';
import {FqCommentCardComponent} from './comments/card-comment/fq-comment-card.component';
import {GravatarPipe} from './pipes/gravatar.pipe';
import {FqPageNavigateComponent} from './page-navigate/fq-page-navigate.component';
import {CommentCreateHttpService} from "../integrations/service/comment-create-http.service";
import {CommentGetHttpService} from "../integrations/service/comment-get-http.service";
import {CommentReplyGetHttpService} from "../integrations/service/comment-reply-get-http.service";
import {CommentReplyCreateHttpService} from "../integrations/service/comment-reply-create-http.service";


@NgModule({
  declarations: [
    FqNavbarComponent,
    FqHomeComponent,
    FqCommentInputComponent,
    FqCommentCardComponent,
    GravatarPipe,
    FqPageNavigateComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports: [
    FqNavbarComponent,
    FqCommentInputComponent,
    FqCommentCardComponent,
    GravatarPipe,
    FqPageNavigateComponent
  ],
  providers: [
    CommentCreateHttpService,
    CommentGetHttpService,
    CommentReplyGetHttpService,
    CommentReplyCreateHttpService
  ]
})
export class FqSharedModule { }
