import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot} from '@angular/router';
import {FqSignUpComponent} from "../../public/components/sign-up/fq-sign-up.component";

@Injectable({
  providedIn: 'root'
})
export class FqFormGuard implements CanDeactivate<FqSignUpComponent> {
  canDeactivate(
    component: FqSignUpComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): boolean {
    if (component.signForm.dirty && !component.submitted) {
      return confirm('Are you sure you want to go out?');
    }
    return true;
  }
}
