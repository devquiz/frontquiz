import {Injectable} from '@angular/core';
import {CanLoad, Route, Router} from '@angular/router';
import {UserStorageService} from "../../integrations/service/user-storage.service";

@Injectable({
  providedIn: 'root'
})
export class FqAuthGuard implements CanLoad {

  constructor(private _router: Router, private _userStorage: UserStorageService
  ) {
  }

  canLoad(route: Route): boolean {
    if (this._userStorage.userValue) {
      return true;
    }
    return false;
  }
}
