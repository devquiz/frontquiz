import {ChangeDetectionStrategy, Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'fq-home',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './fq-home.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class FqHomeComponent {

  constructor(
  ) { }

}
