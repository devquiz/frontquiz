import {Component, ViewEncapsulation} from '@angular/core';
import {User} from 'src/app/integrations/models/User.model';
import {UserStorageService} from 'src/app/integrations/service/user-storage.service';

@Component({
  selector: 'fq-navbar',
  templateUrl: './fq-navbar.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class FqNavbarComponent {

  public currentUser!: User;

  constructor(private _userStorageService: UserStorageService) {
    this._userStorageService.currentUser.subscribe(x => this.currentUser = x);
  }

  public logout(): void {
    this._userStorageService.logout();
  }

}
