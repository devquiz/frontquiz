import {Component, Input, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Comment} from "../../../integrations/models/Comment.model";
import {Subscription} from "rxjs";
import {CommentReplyGetHttpService} from "../../../integrations/service/comment-reply-get-http.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'fq-comment-card',
  templateUrl: './fq-comment-card.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class FqCommentCardComponent implements OnInit, OnDestroy {

  @Input() public comment!: Comment;

  public replyInput: boolean;
  public comments: Comment[];

  private _subscription: Subscription;

  constructor(private _commentReplyGetHttpService: CommentReplyGetHttpService, private _toastr: ToastrService) {
    this.replyInput = false;
    this.comments = [];
    this._subscription = new Subscription();
  }

  ngOnInit(): void {
    this._initialize();
  }

  ngOnDestroy(): void {
    this._finalize();
  }

  public showReplyInput(): void {
    this.replyInput = !this.replyInput;
  }

  public newReply(comment: Comment): void {
    this.comments.push(comment);
    this.replyInput = !this.replyInput;
  }

  private _initialize(): void {
    this._loadCommentsHttpRequest();
  }

  private _finalize(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }

  private _loadCommentsHttpRequest(): void {
    if (this.comment.parentId === null) {
      this._subscription = this._commentReplyGetHttpService.getCommentsByParent(this.comment.commentId)
        .subscribe((res): void => {
          this.comments = res;
          this._subscription.unsubscribe();
        }, error => {
          this._toastr.error(error.error.message);
        });
    }
  }
}
