import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewEncapsulation
} from '@angular/core';
import {Bulletin} from "../../../integrations/models/Bulletin.model";
import {Comment} from "../../../integrations/models/Comment.model";
import {Subscription} from "rxjs";
import {User} from "../../../integrations/models/User.model";
import {UserStorageService} from "../../../integrations/service/user-storage.service";
import {CommentCreateHttpService} from "../../../integrations/service/comment-create-http.service";
import {CommentReplyCreateHttpService} from "../../../integrations/service/comment-reply-create-http.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'fq-comment-input',
  templateUrl: './fq-comment-input.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class FqCommentInputComponent implements OnDestroy {

  @Input() public bulletin!: Bulletin;
  @Input() public isReply: boolean;
  @Input() public commentParent!: Comment;
  @Output() public newComment: EventEmitter<Comment>;

  public currentUser!: User;
  public comment: string;

  private _subscriptionComment: Subscription;
  private _subscriptionReply: Subscription;

  private readonly _TEXT_EMPTY: string = '';

  constructor(private _commentCreateHttpService: CommentCreateHttpService, private _toastr: ToastrService,
              private _commentReplyCreateHttpService: CommentReplyCreateHttpService,
              private _userStorageService: UserStorageService) {
    this._userStorageService.currentUser.subscribe(x => this.currentUser = x);
    this.isReply = false;
    this.newComment = new EventEmitter<Comment>();
    this.comment = this._TEXT_EMPTY;
    this._subscriptionComment = new Subscription();
    this._subscriptionReply = new Subscription();
  }

  ngOnDestroy(): void {
    this._finalize();
  }

  public _onSubmitRequest(): void {
    if (!this.isReply) {
      this._createComment();
    } else {
      this._createReplyComment();
    }
  }

  private _finalize(): void {
    if (this._subscriptionComment || this._subscriptionReply) {
      this._subscriptionComment.unsubscribe();
      this._subscriptionReply.unsubscribe();
    }
  }

  private _createComment(): void {
    if (this.comment !== this._TEXT_EMPTY) {
      this._subscriptionComment = this._commentCreateHttpService.createComment(this.comment,
        this.currentUser.accountId.toString(), this.currentUser.userId.toString(), this.bulletin.bulletinId)
        .subscribe((res): void => {
          this.newComment.emit(res);
          this.comment = this._TEXT_EMPTY;
          this._subscriptionComment.unsubscribe();
        }, error => {
          this._toastr.error(error.error.message);
        });
    }
  }

  private _createReplyComment(): void {
    if (this.comment !== this._TEXT_EMPTY) {
      this._subscriptionReply = this._commentReplyCreateHttpService.replyComment(this.comment,
        this.currentUser.accountId.toString(), this.currentUser.userId.toString(), this.commentParent.commentId)
        .subscribe((res): void => {
          this.newComment.emit(res);
          this.comment = this._TEXT_EMPTY;
          this._subscriptionReply.unsubscribe();
        }, error => {
          this._toastr.error(error.error.message);
        });
    }
  }
}
