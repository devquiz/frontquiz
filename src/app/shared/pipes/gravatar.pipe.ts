import {Pipe, PipeTransform} from '@angular/core';
import {Md5} from "ts-md5";

@Pipe({
  name: 'gravatar',
  pure: true
})
export class GravatarPipe implements PipeTransform {

  transform(value: string | number): string {
    const avatarHash = Md5.hashStr(value + '');
    return `//www.gravatar.com/avatar/${avatarHash}?d=identicon`
  }
}
