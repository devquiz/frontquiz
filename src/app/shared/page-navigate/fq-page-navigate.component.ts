import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'fq-page-navigate',
  templateUrl: './fq-page-navigate.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class FqPageNavigateComponent {

  @Input() public totalPages: number;
  @Input() public pageShow: number;
  @Output() public page: EventEmitter<number>;

  constructor() {
    this.totalPages = 0;
    this.pageShow = 0;
    this.page = new EventEmitter<number>();
  }

  public prevPage(): void {
    this.pageShow -= 1;
    this.page.emit(this.pageShow);
  }

  public nextPage(): void {
    this.pageShow += 1;
    this.page.emit(this.pageShow);
  }
}
