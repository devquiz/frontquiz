import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewEncapsulation
} from '@angular/core';
import {User} from "../../../../integrations/models/User.model";
import {Subscription} from "rxjs";
import {Pagination} from "../../../../integrations/models/Pagination-Bulletin.model";
import {BulletinSearchHttpService} from "../../../../integrations/service/bulletin-search-http.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'fq-bulletin-search',
  templateUrl: './fq-bulletin-search.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FqBulletinSearchComponent implements OnDestroy {

  @Input() public currentUser!: User;
  @Output() public bulletinsFind: EventEmitter<Pagination>;

  public body: string;
  public totalPages: number;
  public page: number;

  private _limit: number;

  private _subscription: Subscription;

  private readonly _TEXT_EMPTY: string = '';

  constructor(private _bulletinSearchHttpService: BulletinSearchHttpService, private _toastr: ToastrService) {
    this.bulletinsFind = new EventEmitter<Pagination>();
    this.body = this._TEXT_EMPTY;
    this.totalPages = 1;
    this.page = 0;
    this._limit = 5;
    this._subscription = new Subscription();
  }

  ngOnDestroy(): void {
    this._finalize();
  }

  public search(page: number): void {
    this._subscription = this._bulletinSearchHttpService.searchBulletins(
      this.currentUser.accountId.toString(), this.currentUser.userId.toString(), this.body, this._limit, page)
      .subscribe((res): void => {
        this.totalPages = res.totalPages;
        this.page = page;
        this.bulletinsFind.emit(res);
        this._subscription.unsubscribe();
      }, error => {
        this._toastr.error(error.error.message);
      });
  }

  public changePage(page: number): void {
    this.page = page;
    this.search(this.page);
  }

  private _finalize(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }
}
