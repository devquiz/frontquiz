import {Component, Input, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Bulletin} from "../../../../integrations/models/Bulletin.model";
import {Comment} from "../../../../integrations/models/Comment.model";
import {Subscription} from "rxjs";
import {CommentGetHttpService} from "../../../../integrations/service/comment-get-http.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'fq-bulletin-card',
  templateUrl: './fq-bulletin-card.component.html',
  encapsulation: ViewEncapsulation.None
})
export class FqBulletinCardComponent implements OnInit, OnDestroy {

  @Input() public bulletin!: Bulletin;

  public writeComment: boolean;
  public comments: Comment[];

  private _subscription: Subscription;

  constructor(private _commentGetHttpService: CommentGetHttpService, private _toastr: ToastrService) {
    this.writeComment = false;
    this.comments = [];
    this._subscription = new Subscription();
  }

  ngOnInit(): void {
    this._initialize();
  }

  ngOnDestroy(): void {
    this._finalize();
  }

  public showCommentInput(): void {
    this.writeComment = !this.writeComment;
  }

  public newComment(comment: Comment): void {
    this.comments.push(comment);
    this.writeComment = !this.writeComment;
  }

  private _initialize(): void {
    this._loadCommentsHttpRequest();
  }

  private _finalize(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }

  private _loadCommentsHttpRequest(): void {
    this._subscription = this._commentGetHttpService.getComments(this.bulletin.bulletinId)
      .subscribe((res): void => {
        this.comments = res.filter(comment => comment.parentId === null);
        this._subscription.unsubscribe();
      }, error => {
        this._toastr.error(error.error.message);
      });
  }


}
