import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import {Subscription} from 'rxjs';
import {User} from 'src/app/integrations/models/User.model';
import {Bulletin} from "../../../../integrations/models/Bulletin.model";
import {BulletinCreateHttpService} from "../../../../integrations/service/bulletin-create-http.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'fq-bulletin-input',
  templateUrl: './fq-bulletin-input.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FqBulletinInputComponent implements OnInit, OnDestroy {

  @Output() public newBulletin: EventEmitter<Bulletin>;
  @Input() public currentUser!: User;

  public text: string;

  private _fileIds: string[];
  private _accountId: string;
  private _subscription: Subscription;

  constructor(private _bulletinCreateHttpService: BulletinCreateHttpService, private _toastr: ToastrService) {
    this.newBulletin = new EventEmitter<Bulletin>();
    this.text = '';
    this._fileIds = ['1', '2', '3'];
    this._accountId = '1';
    this._subscription = new Subscription();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this._finalize();
  }

  public _onSubmitRequest(): void {
    if(this.text !== ''){
      this._subscription = this._bulletinCreateHttpService.createBulletin(this.text, this._fileIds,
        this.currentUser.userId.toString(), this._accountId)
        .subscribe((res): void => {
          this.newBulletin.emit(res);
          this.text = '';
          this._subscription.unsubscribe();
        }, error => {
          this._toastr.error(error.error.message);
      });
    }
  }

  private _finalize(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }
}
