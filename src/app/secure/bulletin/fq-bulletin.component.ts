import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {User} from 'src/app/integrations/models/User.model';
import {UserStorageService} from 'src/app/integrations/service/user-storage.service';
import {Bulletin} from "../../integrations/models/Bulletin.model";
import {Subscription} from "rxjs";
import {BulletinGetHttpService} from "../../integrations/service/bulletin-get-http.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'fq-bulletin',
  templateUrl: './fq-bulletin.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class FqBulletinComponent implements OnInit, OnDestroy {

  public currentUser!: User;
  public bulletins: Bulletin[];

  private _subscription: Subscription;

  constructor(private _userStorage: UserStorageService, private _bulletinGetHttpService: BulletinGetHttpService,
              private _toastr: ToastrService) {
    this._userStorage.currentUser.subscribe(x => this.currentUser = x);
    this.bulletins = [];
    this._subscription = new Subscription();
  }

  ngOnInit(): void {
    this._initialize();
  }

  ngOnDestroy(): void {
    this._finalize();
  }

  public newBulletinInput(bulletin: Bulletin): void {
    this.bulletins.push(bulletin);
  }

  public bulletinsFind(bulletins: any): void {
    this.bulletins = bulletins.content.reverse();
  }

  private _initialize(): void {
    this.getBulletins();
  }

  private _finalize(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }

  private getBulletins(): void {
    this._subscription = this._bulletinGetHttpService.getBulletins().subscribe((res): void => {
      this.bulletins = res;
      this._subscription.unsubscribe();
    }, error => {
      this._toastr.error(error.error.message);
    });
  }
}
