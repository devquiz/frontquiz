import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FqBulletinRoutingModule} from './fq-bulletin-routing.module';
import {FqBulletinComponent} from './fq-bulletin.component';
import {FqBulletinInputComponent} from './components/new-bulletin/fq-bulletin-input.component';
import {FqBulletinCardComponent} from './components/card-bulletin/fq-bulletin-card.component';
import {FormsModule} from '@angular/forms';
import {FqBulletinSearchComponent} from './components/search-bulletin/fq-bulletin-search.component';
import {FqSharedModule} from "../../shared/fq-shared.module";
import {BulletinCreateHttpService} from "../../integrations/service/bulletin-create-http.service";
import {BulletinGetHttpService} from "../../integrations/service/bulletin-get-http.service";
import {BulletinSearchHttpService} from "../../integrations/service/bulletin-search-http.service";


@NgModule({
  declarations: [
    FqBulletinComponent,
    FqBulletinInputComponent,
    FqBulletinCardComponent,
    FqBulletinSearchComponent
  ],
  imports: [
    CommonModule,
    FqBulletinRoutingModule,
    FormsModule,
    FqSharedModule
  ],
  providers: [
    BulletinCreateHttpService,
    BulletinGetHttpService,
    BulletinSearchHttpService
  ]
})
export class FqBulletinModule {
}
