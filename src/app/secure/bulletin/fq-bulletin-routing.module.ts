import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FqBulletinComponent} from "./fq-bulletin.component";

const routes: Routes = [
  {path: '', component: FqBulletinComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FqBulletinRoutingModule { }
