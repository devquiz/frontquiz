import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FqPublicRoutingModule} from './fq-public-routing.module';
import {FqSignUpComponent} from "./sign-up/fq-sign-up.component";
import {FqPublicComponent} from './fq-public.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FqSignInComponent} from './sign-in/fq-sign-in.component';
import {SignInHttpService} from "../../integrations/service/sign-in-http.service";
import {SignUpHttpService} from "../../integrations/service/sign-up-http.service";


@NgModule({
  declarations: [
    FqSignUpComponent,
    FqPublicComponent,
    FqSignInComponent
  ],
  imports: [
    CommonModule,
    FqPublicRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    SignInHttpService,
    SignUpHttpService
  ]
})
export class FqPublicModule { }
