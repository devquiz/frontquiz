import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FqSignUpComponent} from "./sign-up/fq-sign-up.component";
import {FqPublicComponent} from "./fq-public.component";
import {FqFormGuard} from 'src/app/shared/guards/fq-form.guard';
import {FqSignInComponent} from "./sign-in/fq-sign-in.component";

const routes: Routes = [
  {
    path: '', component: FqPublicComponent,
    children: [
      {path: 'sign-in', component: FqSignInComponent, canDeactivate: [FqFormGuard]},
      {path: 'sign-up', component: FqSignUpComponent, canDeactivate: [FqFormGuard]}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FqPublicRoutingModule { }
