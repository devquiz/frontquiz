import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {first} from "rxjs/operators";
import {Subscription} from "rxjs";
import {SignUpHttpService} from 'src/app/integrations/service/sign-up-http.service';
import {UserStorageService} from "../../../integrations/service/user-storage.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'fq-sign-up',
  templateUrl: './fq-sign-up.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FqSignUpComponent implements OnInit, OnDestroy {

  public title: string;
  public signForm!: FormGroup;
  public submitted: boolean;

  private _subscription: Subscription;

  constructor(private _router: Router, private _signUpHttpService: SignUpHttpService, private _toastr: ToastrService,
              private _formBuilder: FormBuilder, private _userStorage: UserStorageService) {
    this.submitted = false;
    this.title = 'Sign Up';
    this._subscription = new Subscription();
  }

  ngOnInit(): void {
    this._initialize();
  }

  ngOnDestroy(): void {
    this._finalize();
  }

  public get form(): { [key: string]: AbstractControl } {
    return this.signForm.controls;
  }

  public _signUpRequest(): void {
    this.submitted = true;
    if (this.signForm.invalid){
      return;
    }
    this._subscription = this._signUpHttpService.signUp(this.signForm.value)
      .pipe(first())
      .subscribe((user): void => {
        this._userStorage.currentUserSubject.next(user);
        this._userStorage.saveUser(user);
        this._toastr.success('User created successfully');
        this._router.navigate(['bulletins']);
        this._subscription.unsubscribe();
      }, (): void => {
        this._toastr.error('First Name or Last Name already registered');
      });
  }

  private _initialize(): void {
    this._createForm();
  }

  private _finalize(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }

  private _createForm(): void {
    this.signForm = this._formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      lastName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      accountId: ['1']
    });
  }

}
