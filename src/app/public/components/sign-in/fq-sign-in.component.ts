import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";
import {UserStorageService} from "../../../integrations/service/user-storage.service";
import {first} from "rxjs/operators";
import {SignInHttpService} from "../../../integrations/service/sign-in-http.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'fq-sign-in',
  templateUrl: './fq-sign-in.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FqSignInComponent implements OnInit, OnDestroy {

  public title: string;
  public signForm!: FormGroup;
  public submitted: boolean;

  private _subscription: Subscription;

  constructor(private _router: Router, private _signInHttpService: SignInHttpService, private _toastr: ToastrService,
              private _formBuilder: FormBuilder, private _userStorage: UserStorageService) {
    this.title = 'Sign In';
    this.submitted = false;
    this._subscription = new Subscription();
  }

  ngOnInit(): void {
    this._initialize();
  }

  ngOnDestroy(): void {
    this._finalize();
  }

  public get form(): { [key: string]: AbstractControl } {
    return this.signForm.controls;
  }

  public _signInRequest(): void {
    this.submitted = true;
    if (this.signForm.invalid) {
      return;
    }
    this._subscription = this._signInHttpService.signIn(this.signForm.value)
      .pipe(first())
      .subscribe((user): void => {
        this._userStorage.currentUserSubject.next(user);
        this._userStorage.saveUser(user);
        this._router.navigate(['bulletins']);
        this._subscription.unsubscribe();
      }, error => {
        this._toastr.error(error.error.message);
      });
  }

  private _initialize(): void {
    this._createForm();
  }

  private _finalize(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }

  private _createForm(): void {
    this.signForm = this._formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      lastName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      accountId: ['1']
    });
  }
}
